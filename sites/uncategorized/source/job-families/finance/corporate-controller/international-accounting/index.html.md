---
layout: job_family_page
title: "International Accounting"
description: " _add high level description of job family_ "
---

## Levels

### Director, International Accounting

The Director, International Accounting  is responsible for the leadership, strategy, and execution of International Accounting, including developing policies and procedures to enable the attainment of key corporate and business objectives.

This role reports to the [VP, Corporate Controller](https://about.gitlab.com/job-families/finance/corporate-controller).

#### Director, International Accounting Job Grade

The Director, International Accounting is a [10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, International Accounting Responsibilities

- Collaborate with local service providers for each operating entity to ensure the compilation of the statutory financials, collaborating with the Accounting team
- Identify and calculate US GAAP to Local GAAP adjustments
- Collaborate with the GL team on intercompany balances
- Responsible for account recons (Due to / from accounts, TP, AR and AP)
- Review of IC account activity to ensure all accounts net to zero / TP accounts match, Review of IC AR / AP
- FX review to determine if there are “open” foreign currency balances that are being revalued to adjust as needed
- Collaborate with the Tax Team as needed for purposes of Stat to Tax Analysis, Intercompany contributions, Transfer Pricing related activities
- Ensure correct JE’s to reflect the intercompany contributions and loan offsets
- Ownership of transfer pricing calculations
- Ownership of AP reclasses, Computer equipment, Movement of cash
- Collaborate with Treasury on projects of relating to intercompany funding, intercompany contributions
- Perform analysis to determine the impact of e.g. new accounting standards, changes in accounting policies, impact of non-GAAP policies on statutory reporting requirements for the non-US entities
- Initiate design and implementation of new policies and procedures for non-US entities to support new audit requirements and business activities in non-US jurisdictions
- Drive the implementation of any tools to improve financial reporting for current and future non-US entities
- Ensure all disclosures are properly included in the financial statements of non-US entities
- Identify the need for resources, hire and train new staff
- People manager of one or more team members
- Managing India entity 
- Implement internal controls and maintain SOX compliance


#### Director, International Accounting Requirements

- 15+ years of progressive growth in financial management roles in high growth companies, Software/SaaS industry experience is preferred
- 3+ years in a finance international role, leading various finance operations functions
- Knowledge of Netsuite preferred
- You share our values, and work in accordance with those values
- Leadership at GitLab
- Ability to use GitLab


#### Director, International Accounting Performance Indicators

- Stat audit completion on time
- Value adding and building efficiencies on cross-functional relationships within the company in setting up new foreign entities
- Clean inter-company transactions and processes, including compliance with Inter-company Transfer Pricing agreements, as well as the completeness and accuracy of the Inter-company accounts, and eliminations
- Improvement of automation and efficiency in the consolidation and reporting process




### Senior Director, International Accounting

The Director, International Accounting  is responsible for the leadership, strategy, and execution of International Accounting, including developing policies and procedures to enable the attainment of key corporate and business objectives.

This role reports to the [VP, Corporate Controller](https://about.gitlab.com/job-families/finance/corporate-controller).

#### Senior Director, International Accounting Job Grade

The Senior Director, International Accounting is a [11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Director, International Accounting Responsibilities

- Collaborate with local service providers for each operating entity to ensure the compilation of the statutory financials, collaborating with the Accounting team
- Identify and calculate US GAAP to Local GAAP adjustments
- Collaborate with the GL team on intercompany balances
- Responsible for account recons (Due to / from accounts, TP, AR and AP)
- Review of IC account activity to ensure all accounts net to zero / TP accounts match, Review of IC AR / AP
- FX review to determine if there are “open” foreign currency balances that are being revalued to adjust as needed
- Collaborate with the Tax Team as needed for purposes of Stat to Tax Analysis, Intercompany contributions, Transfer Pricing related activities
- Ensure correct JE’s to reflect the intercompany contributions and loan offsets
- Ownership of transfer pricing calculations
- Ownership of AP reclasses, Computer equipment, Movement of cash
- Collaborate with Treasury on projects of relating to intercompany funding, intercompany contributions
- Perform analysis to determine the impact of e.g. new accounting standards, changes in accounting policies, impact of non-GAAP policies on statutory reporting requirements for the non-US entities
- Initiate design and implementation of new policies and procedures for non-US entities to support new audit requirements and business activities in non-US jurisdictions
- Drive the implementation of any tools to improve financial reporting for current and future non-US entities
- Ensure all disclosures are properly included in the financial statements of non-US entities
- Identify the need for resources, hire and train new staff
- People manager of one or more team members
- Managing India entity 
- Implement internal controls and maintain SOX compliance
- Ensure compliance with intercompany pricing agreements and other intercompany accounts
- Complete monthly reconciliations, variance and Ad Hoc analysis and audit requests
- Implement and streamline accounting policies and procedures, as well as reporting functions
- Collaborate closely with key stakeholders, internal/external customers, IT and Operations daily


#### Senior Director, Corporate Controller Requirements

- 20+ years of progressive growth in financial management roles in high growth companies, Software/SaaS industry experience is preferred
- 7+ years in a finance operations role, leading various finance operations functions
- Knowledge of Coupa and Workday preferred
- You share our values, and work in accordance with those values
- Leadership at GitLab
- Ability to use GitLab


#### Senior Director, Corporate Controller Performance Indicators

- Stat audit completion on time
- Value adding and building efficiencies on cross-functional relationships within the company in setting up new foreign entities
- Clean inter-company transactions and processes, including compliance with Inter-company Transfer Pricing agreements, as well as the completeness and accuracy of the Inter-company accounts, and eliminations
- Improvement of automation and efficiency in the consolidation and reporting process


## Career Ladder
The next step for the International Accounting  job family is not yet defined.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30min screening call with one of our Global Recruiters
- Next, candidates will be invited to schedule a first interview with the Hiring Manager
- Next, candidates will be invited to interview with 2-5 team members
- There may be a final executive interview


Additional details about our process can be found on our [hiring page](/handbook/hiring/).



